package com.altemora.firsttest.ui.fragments

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil

import com.altemora.firsttest.R
import com.altemora.firsttest.databinding.FragmentDetailsBinding
import com.altemora.firsttest.ui.activities.MainActivity
import com.altemora.firsttest.ui.dto.DataItem

class DetailsFragment : Fragment(), MainActivity.FragmentDataListener {
    private lateinit var binding: FragmentDetailsBinding;

    companion object {
        fun newInstance() = DetailsFragment()
    }

    private lateinit var viewModel: DetailsFragmentViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_details, container, false)
        binding.item = DataItem("", "", "", "", "", "")
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(DetailsFragmentViewModel::class.java)
    }

    fun getListener(): MainActivity.FragmentDataListener {
        return this;
    }

    override fun onSendDataItem(item: DataItem) {
        binding.item = item;
    }

}
