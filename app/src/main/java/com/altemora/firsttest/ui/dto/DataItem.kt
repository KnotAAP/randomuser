package com.altemora.firsttest.ui.dto

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso

data class DataItem(
    val name: String,
    val login: String,
    val photo: String?,
    val email: String,
    val age: String,
    val address: String
)

@BindingAdapter("url")
fun bindImage(view: ImageView, url: String) {
    if (!url.isEmpty())
        Picasso.get()
            .load(url)
            .fit()
            .into(view)
}
