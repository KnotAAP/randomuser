package com.altemora.firsttest.ui.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.altemora.firsttest.R
import com.altemora.firsttest.ui.dto.DataItem
import com.altemora.firsttest.ui.fragments.DetailsFragment
import com.altemora.firsttest.ui.fragments.ItemListFragment

class MainActivity : AppCompatActivity(R.layout.activity_main) {
    lateinit var itemList: ItemListFragment
    lateinit var details: DetailsFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        itemList = ItemListFragment.newInstance()
        details = DetailsFragment.newInstance()
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.listFragment, itemList)
            .replace(R.id.detailsFragment, details)
            .commit()
        itemList.setListener(details.getListener())
    }

    interface FragmentDataListener {
        fun onSendDataItem(item: DataItem)
    }
}
