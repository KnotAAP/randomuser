package com.altemora.firsttest.ui.fragments

import android.content.Context
import android.view.View
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.altemora.firsttest.R
import com.altemora.firsttest.ui.dto.DataItem
import com.altemora.firsttest.data.Repository
import com.altemora.firsttest.ui.activities.MainActivity
import com.altemora.firsttest.ui.adapters.ItemsAdapter

class ItemListViewModel : ViewModel(), Repository.OnItemsList, ItemsAdapter.Callback {
    private val repository = Repository()
    private lateinit var adapter: ItemsAdapter;
    private lateinit var fragmentListener: MainActivity.FragmentDataListener;

    fun getDataItems() {
        repository.getItemsList(this)
    }

    fun setFragmentDataListener(listener: MainActivity.FragmentDataListener) {
        fragmentListener = listener
    }

    fun createAdapter(context: Context, view: View) {
        adapter = ItemsAdapter(ArrayList(), this)
        val recycler = view?.findViewById<RecyclerView>(R.id.list_dataitems)
        val decorator = DividerItemDecoration(context, LinearLayoutManager.VERTICAL)
        decorator.setDrawable(context?.getDrawable(R.drawable.divider)!!)
        recycler?.addItemDecoration(decorator)
        recycler?.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recycler?.adapter = adapter
    }

    override fun onItemsSuccess(list: ArrayList<DataItem>) {
        adapter.items = list
        adapter.notifyDataSetChanged()
    }

    override fun onItemsError(error: Throwable) {
        //TODO:something
    }

    override fun onItemClicked(context: Context, item: DataItem) {
        fragmentListener.onSendDataItem(item)
    }
}
