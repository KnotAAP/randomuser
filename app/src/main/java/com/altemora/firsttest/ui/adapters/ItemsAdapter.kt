package com.altemora.firsttest.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.altemora.firsttest.R
import com.altemora.firsttest.databinding.ItemDataitemBinding
import com.altemora.firsttest.ui.adapters.ItemsAdapter.ItemViewHolder
import com.altemora.firsttest.ui.dto.DataItem


class ItemsAdapter(var items: List<DataItem>, val callback: Callback) :
    RecyclerView.Adapter<ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder =
        ItemViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_dataitem,
                parent,
                false
            )
        )


    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(items[position])
    }

    inner class ItemViewHolder(private val binding: ItemDataitemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: DataItem) {
            binding.item = item
            binding.executePendingBindings()
            itemView.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) callback.onItemClicked(
                    itemView.context,
                    items[adapterPosition]
                )
            }
        }
    }

    interface Callback {
        fun onItemClicked(context: Context, item: DataItem)
    }
}