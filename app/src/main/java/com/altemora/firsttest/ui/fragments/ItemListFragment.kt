package com.altemora.firsttest.ui.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.altemora.firsttest.R
import com.altemora.firsttest.ui.activities.MainActivity
import com.altemora.firsttest.ui.adapters.ItemsAdapter
import com.altemora.firsttest.ui.dto.DataItem


class ItemListFragment : Fragment() {
    private lateinit var listener: MainActivity.FragmentDataListener;

    companion object {
        fun newInstance() = ItemListFragment()
    }

    private lateinit var viewModel: ItemListViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_item_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ItemListViewModel::class.java)
        viewModel.createAdapter(context!!, view!!)
        viewModel.getDataItems()
        viewModel.setFragmentDataListener(listener)
    }

    fun setListener(listener: MainActivity.FragmentDataListener) {
        this.listener = listener
    }
}
