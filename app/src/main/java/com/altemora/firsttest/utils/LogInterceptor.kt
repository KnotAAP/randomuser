package com.altemora.firsttest.utils

import android.util.Log
import okhttp3.Interceptor
import okhttp3.Response
import okio.Buffer
import java.io.IOException
import java.nio.charset.Charset
import java.nio.charset.UnsupportedCharsetException
import java.util.concurrent.TimeUnit


class LogInterceptor : Interceptor {
    private val UTF8 = Charset.forName("UTF-8")
    private var SHOW_LOG = true
    private var SHOW_HEADER = false


    private fun log(message: String) {
        Log.e("Server", message)
    }

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        if (!SHOW_LOG) {
            return chain.proceed(request)
        }
        //@REQUEST Title
        val requestBody = request.body()
        val requestTitleMessage = "@" + request.method() + ' ' + request.url()
        Log.e(" ", " ")
        log(requestTitleMessage)
        //@REQUEST Header
        if (SHOW_HEADER) {
            val headers = request.headers()
            var i = 0
            val count = headers.size()
            while (i < count) {
                val name = headers.name(i)
                if (!"Content-Type".equals(
                        name,
                        ignoreCase = true
                    ) && !"Content-Length".equals(name, ignoreCase = true)
                ) {
                    log(name + ": " + headers.value(i))
                }
                i++
            }
        }
        //@REQUEST Body
        if (requestBody != null) {
            val buffer = Buffer()
            requestBody.writeTo(buffer)
            var charset = UTF8
            val contentType = requestBody.contentType()
            if (contentType != null) {
                charset = contentType.charset(UTF8)
            }
            log("BODY: " + buffer.readString(charset))
        }
        val startNs = System.nanoTime()
        val response: Response
        response = try {
            chain.proceed(request)
        } catch (e: Exception) {
            log("@HTTP FAILED: $e")
            //WARNING
            throw e
        }
        //FINISH REQUEST
        val tookMs =
            TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startNs)
        //@RESPONSE Title
        val responseBody = response.body()
        val contentLength = responseBody!!.contentLength()
        log(
            "@RESPONSE   " + response.code() + ' ' + response.message() + ' '
                    + response.request().url() + " (" + tookMs + "ms" + ')'
        )
        //@RESPONSE Header
        if (SHOW_HEADER) {
            val headers2 = response.headers()
            var i = 0
            val count = headers2.size()
            while (i < count) {
                log(headers2.name(i) + ": " + headers2.value(i))
                i++
            }
        }
        //@RESPONSE Body
        val source = responseBody.source()
        source.request(Long.MAX_VALUE) // Buffer the entire body.
        val buffer = source.buffer()
        var charset = UTF8
        val contentType = responseBody.contentType()
        if (contentType != null) {
            charset = try {
                contentType.charset(UTF8)
            } catch (e: UnsupportedCharsetException) {
                log("Couldn't decode the response body; charset is likely malformed.")
                log("@END RESPONSE")
                return response
            }
        }
        if (contentLength != 0L) {
            log("BODY: " + buffer.clone().readString(charset))
        }
        log("@END RESPONSE")
        return response
    }
}
