package com.altemora.firsttest.data

import android.annotation.SuppressLint
import com.altemora.firsttest.data.randomuser.Api
import com.altemora.firsttest.ui.dto.DataItem

class Repository {
    @SuppressLint("CheckResult")
    fun getItemsList(callback: OnItemsList) {
        Api().getUsersList(callback)
    }

    interface OnItemsList {
        fun onItemsSuccess(list: ArrayList<DataItem>)
        fun onItemsError(error: Throwable)
    }
}