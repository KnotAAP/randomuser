package com.altemora.firsttest.data.randomuser.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ApiDate(
    @Expose @SerializedName("data") val data: String,
    @Expose @SerializedName("age") val age: String
)