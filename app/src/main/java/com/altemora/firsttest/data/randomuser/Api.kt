package com.altemora.firsttest.data.randomuser

import android.annotation.SuppressLint
import android.util.Log
import com.altemora.firsttest.data.Repository
import com.altemora.firsttest.data.randomuser.dto.Result
import com.altemora.firsttest.ui.dto.DataItem
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class Api {
    @SuppressLint("CheckResult")
    fun getUsersList(callback: Repository.OnItemsList) {
        val api = ApiProvider.api()
        //createing request pull for 10 users
        val requests: ArrayList<Observable<Result>> = ArrayList()
        for (i in 1..10) {
            requests.add(
                api.getUsers().observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io()).delaySubscription(
                        (250 * i).toLong(),
                        TimeUnit.MILLISECONDS
                    )//time interval for api
            )
        }
        //zip for one result
        Observable.zip(requests) {
            var result = Result(ArrayList());
            //concat all results to one
            it.forEach {
                (it as Result).results?.let { it1 -> result.results?.addAll(it1) }
            }
            return@zip result;
        }.subscribe({ it ->
            Log.e("TAG", "Success!")
            val list = ArrayList<DataItem>()
            for (user in it.results!!) {
                list.add(
                    DataItem(
                        user.name.title + ". " + user.name.first + " " + user.name.last,
                        user.login.username,
                        user.picture.large,
                        user.email,
                        user.dob.age,
                        user.country + ", " + user.state + ", " + user.city + ", " + user.postcode
                    )
                )
            }
            callback.onItemsSuccess(list)
        }) {
            Log.e("TAG", "Error", it)
            callback.onItemsError(it)
        }
    }
}