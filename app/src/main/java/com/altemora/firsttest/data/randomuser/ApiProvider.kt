package com.altemora.firsttest.data.randomuser

import com.altemora.firsttest.utils.LogInterceptor
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


object ApiProvider {
    private const val URL = "https://randomuser.me/"

    fun api(): ApiInterface {
        val httpClient = OkHttpClient.Builder()
        val logging = LogInterceptor()
        httpClient.addInterceptor(logging)
        httpClient.connectTimeout(15, TimeUnit.SECONDS)
        httpClient.readTimeout(45, TimeUnit.SECONDS)
        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl(URL)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().excludeFieldsWithoutExposeAnnotation().disableHtmlEscaping().create()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(httpClient.build())
            .build()
        return retrofit.create(ApiInterface::class.java)
    }

}