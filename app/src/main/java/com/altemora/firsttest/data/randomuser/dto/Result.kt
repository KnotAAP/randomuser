package com.altemora.firsttest.data.randomuser.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Result(
    @Expose @SerializedName("results") val results: ArrayList<User>?
)