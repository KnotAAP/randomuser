package com.altemora.firsttest.data.randomuser

import com.altemora.firsttest.data.randomuser.dto.Result
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET


interface ApiInterface {
    @GET("api/")
    fun getUsers(): Observable<Result>
}