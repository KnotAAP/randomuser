package com.altemora.firsttest.data.randomuser.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import org.json.JSONObject

data class User(
    @Expose @SerializedName("id") val id: Id,
    @Expose @SerializedName("name") val name: Name,
    @Expose @SerializedName("login") val login: Login,
    @Expose @SerializedName("picture") val picture: Picture,
    @Expose @SerializedName("gender") val gender: String,
    @Expose @SerializedName("location") val location: JSONObject,
    @Expose @SerializedName("city") val city: String,
    @Expose @SerializedName("state") val state: String,
    @Expose @SerializedName("country") val country: String,
    @Expose @SerializedName("postcode") val postcode: String,
    @Expose @SerializedName("email") val email: String,
    @Expose @SerializedName("phone") val phone: String,
    @Expose @SerializedName("dob") val dob: ApiDate,
    @Expose @SerializedName("registered") val registered: ApiDate
)
